[KIV/OPSWI] Rozbor open source portálů z pohledu dat o projektech
=================================================================

## Zadání ##
Napsat [jednoúčelový] nástroj, který poskytne rozhraní pro výběr projektů z OSS portálů tak, aby pomocí něj šlo ve velkém objemu projektů vyhledávat podmnožiny vhodné pro další zkoumání

**Téma vypsal:** Doc. Ing. Přemysl Brada, MSc., Ph.D.

## Struktura projektu - ChpCrawler (Chp - Code hosting platforms) ##

* **Crawlers** - obsahuje třídy pro získávání strukturovaných dat z OSS portálů (GitHub a GitLab). 
** GitHubScraper
** GitLabScraper
** IScraper
* **db** - obsahuje třídy pro ukládaní dat do csv souboru
** CsvExport
** IDatabase
* **Models** - obsahuje třídy pro prací se Scraperem
** Project - instance projektu
** UrlHelper - třída pro formátování URL 
* **App.config** - konfigurace

## Konfigurace - App.config ##


```xml
		<add key="PageLimit" value="1" />				  <!-- Max number of page to process -->
		<add key="DateLimit" value="2020-W01-1" />		  <!-- Last activity after, ISO 8601 (YYYY-MM-DDTHH:MM:SSZ) (!ONLY FOR GITLAB!) -->
		<add key="LimitIssues" value="10" />			  <!-- Min number of issues -->
		<add key="LimitStars" value="2" />				  <!-- Min number of stars/watchers -->
		<add key="ProjectsPerPage" value="100" />		  <!-- Max 100 projects on 1 page -->
		<add key="SleepTime" value="5000" />              <!-- Sleep before processing next project -->
		<add key="GithubUsername" value="pwnsauce8" />    <!-- Github username -->
		<add key="RunGitHub" value="1" />				  <!-- 1 - true; 0 - false -->
		<add key="RunGitLab" value="1" />				  <!-- 1 - true; 0 - false -->
		<add key="GitHubAccessToken" value="***" />	                <!-- GitHub access token -->
		<add key="GitLabAccessToken" value="***" />					<!-- GitLab access token -->
		<add key="CsvExportPath" value="C:\Users\42077\Documents\Test\export.csv" />		<!-- Export Path -->
```

* **PageLimit** - maximální počet stránek ke zpracování
* **DateLimit** - Vybere všechny projekty od datumu
	* Konfigurace plati pouze pro GitLab
* **LimitIssues** - Filtruje projekty které mají víc než X issue
* **LimitStars** - Filtruje projekty které mají víc než X stars
* **ProjectsPerPage** - Počet projektů na 1 stránku. Max 100
* **SleepTime** - Nastavuje "přestávku" mezi zpracovanými projekty 
	* Bez přestavek občas vzniká chyba Forbidden 403
	* Čím je větší hodnota, tím je pomalejší běh programu
* **GithubUsername** - Váš username na Githubu
* **RunGitHub** - Provést export GitHubu? (1 - true; 0 - false)
* **RunGitLab** - Provést export GitLabu? (1 - true; 0 - false)
* **GitHubAccessToken** - Github access token ([Guide](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token))
* **GitLabAccessToken** - Gitlab access token ([Guide](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html))
* **CsvExportPath** - Cesta k csv souboru 

## Data 

Daný projekt sbírá následující data z portálu GitHub a GitLab. 

* **Id** - id projektu
* **OwnerName** - username majitele projektu
* **RepoName** - název repozitáře
* **CodeHostingPlatform** - Github/Gitlab
* **Url** - url projektu
* **License** - Licence projektu
* **NumOfCommit** - Počet commitů
* **NumOfContributors** - Počet přispěvatelů   
* **NumOfIssuesAll** - Celkový počet issues
* **NumOfIssuesOpen** - Počet otevřených issues
* **NumOfIssuesClose** - Počet vyřešených issues
* **NumOfStars** - Počet hvězdiček na projektu
* **NumOfForks** - Počet forks
* **NumOfReleases** - Počet releasů

## Poznámky

* License chybí u GitLabu z důvodu absence této informace v API 
* Počet commitů a počet releasů se získává pomocí web scrapingu. 
	* V případě změny HTML Githubu a Gitlabu nemusí fungovat

## API

* **API GitHub** - https://docs.github.com/en/rest
* **API GitLab** - https://docs.gitlab.com/ee/api/