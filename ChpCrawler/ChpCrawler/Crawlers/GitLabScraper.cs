﻿using ChpCrawler.db;
using ChpCrawler.Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using HtmlAgilityPack;
using System.Threading;

namespace ChpCrawler.Crawlers
{
    // https://docs.gitlab.com/ee/api/
    class GitLabScraper : IScraper
    {
        private readonly CodeHostingPlatformEnum DeafultPlatform = CodeHostingPlatformEnum.GitLab;
        private string ProjectsUrl;
        private readonly string ContributersUrl = "projects/*PROJECTN*/repository/contributors?page=*PAGE*";

        private int LIMIT;
        private string LIMIT_DATE;
        private int LIMIT_ISSUES;
        private int LIMIT_STARS;
        private int PROJECTS_PER_PAGE;
        private int SLEEP_TIME;

        public string GitLabAccessToken { get; set; }
        public string BaseUrl { get; set; }
        public IDatabase Storage { get; set; }

        public GitLabScraper(){}

        public void Config(int page_limit, int issue_count_limit, int stars_count_limit, string date_limit, int projects_per_page, int sleep_time)
        {
            LIMIT = page_limit;
            LIMIT_DATE = date_limit;
            LIMIT_ISSUES = issue_count_limit;
            LIMIT_STARS = stars_count_limit;
            PROJECTS_PER_PAGE = projects_per_page;
            SLEEP_TIME = sleep_time;
            ProjectsUrl = $"projects?last_activity_after={date_limit}?pagination=keyset&per_page={projects_per_page}&order_by=id&sort=asc&";
        }

        public void ExtractProjects(bool use_issue_num_limit = true, bool use_only_starred = true)
        {
            int limit = LIMIT;
            bool canContinue = true;
            string currentUrl = UrlHelper.BuildUrl(new string[] {
                                    BaseUrl,
                                    ProjectsUrl + "id_after=*LASTID*"
                                });
                        
            string lastId = null;

            while (canContinue && limit != 0)
            {
                try
                {
                    string json = lastId == null ? GetJson(BaseUrl + ProjectsUrl) : GetJson(currentUrl.Replace("*LASTID*", lastId));
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);

                    foreach (var obj in jsonObj)
                    {
                        Project project;

                        try
                        {
                            project = FillProject(obj);
                        }
                        catch (System.Net.WebException ex)
                        {
                            Console.WriteLine(ex.Message);
                            Thread.Sleep(SLEEP_TIME);
                            continue;
                        }

                        Thread.Sleep(SLEEP_TIME);
                        if (use_issue_num_limit && project.NumOfIssuesAll < LIMIT_ISSUES) continue;
                        if (use_only_starred && project.NumOfStars < LIMIT_STARS) continue;

                        Storage.Insert(project);
                    }
                    lastId = jsonObj.Last.id;
                    limit--;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    canContinue = false;
                }
            }
        }

        private Project FillProject(dynamic obj)
        {
            Project project = new Project()
            {
                Id = Convert.ToString(obj.id),
                RepoName = Convert.ToString(obj.name_with_namespace),
                CodeHostingPlatform = DeafultPlatform,
                Url = Convert.ToString(obj.http_url_to_repo),
                License = obj.license != null ? Convert.ToString(obj.license.name) : "",
                NumOfCommit = GetNumOfCommits(Convert.ToString(obj.http_url_to_repo)),
                NumOfStars = obj.star_count != null ? Convert.ToInt32(obj.star_count) : 0,
                OwnerName = obj["namespace"] != null ? Convert.ToString(obj["namespace"].name) : "",
                NumOfForks = Convert.ToInt32(obj.forks_count)
            };
            project.NumOfContributors = GetContributersCount(ContributersUrl, project.Id);
            GetIssueStatistis(project);
            GetNumOfReleases(project);

            return project;
        }

        private int GetNumOfCommits(string url)
        {
            try
            {
                HtmlWeb web = new HtmlWeb();
                HtmlDocument document = web.Load(url);
                if (document != null)
                {
                    HtmlNode node = document.DocumentNode.SelectSingleNode("//div[@class='project-home-panel js-show-on-project-root gl-my-5']//li[1]//strong[@class='project-stat-value']");
                    string value = node != null ? node.InnerText : "0";
                    return Int32.Parse(value.Replace(",", ""));
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Commit error");
            }

            return 0;
        }

        private void GetNumOfReleases(Project project)
        {
            HtmlWeb web = new HtmlWeb();
            int result = 0;
            HtmlDocument document = web.Load(project.Url);
            if (document != null)
            {
                HtmlNode node = document.DocumentNode.SelectSingleNode("//div[@class='project-home-panel js-show-on-project-root gl-my-5']//li[5]//a[1]");
                string value = node != null ? node.InnerText.Replace("Releases", string.Empty).Replace("Release", string.Empty).Replace(" ", string.Empty) : "0";
                result = Int32.Parse(value.Replace(",", ""));
               
            }
            project.NumOfReleases = result;
        }

        private int GetContributersCount(string url, string project_id)
        {
            int count = 0;
            int page_count = 1;
            string json;

            while (true)
            {
                json = GetJson(
                                UrlHelper.BuildUrl(new string[] {
                                    BaseUrl,
                                    url.Replace("*PROJECTN*", project_id).Replace("*PAGE*", page_count.ToString())
                                }));
                dynamic jsonObj = JsonConvert.DeserializeObject(json);
                if (jsonObj.Count == 0) break;
                count++;
                page_count++;
            }

            return count;
        }

        private void GetIssueStatistis(Project project)
        {
            string json = GetJson(
                                     UrlHelper.BuildUrl(new string[] {
                                        BaseUrl,
                                        $"projects/{project.Id}/issues_statistics"
                                     }));

            dynamic jsonObj = JsonConvert.DeserializeObject(json);

            project.NumOfIssuesAll = Convert.ToInt32(jsonObj.statistics.counts.all); 
            project.NumOfIssuesOpen = Convert.ToInt32(jsonObj.statistics.counts.closed);
            project.NumOfIssuesClose = Convert.ToInt32(jsonObj.statistics.counts.opened);
        }

        public string GetJson(string url)
        {
            string json;

            var httpRequest = (HttpWebRequest)WebRequest.Create(url);

            httpRequest.Accept = "application/json";
            httpRequest.Headers["Authorization"] = $"Bearer {GitLabAccessToken}";

            var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                json = streamReader.ReadToEnd();
            }
            return json;
        }

    }
}
