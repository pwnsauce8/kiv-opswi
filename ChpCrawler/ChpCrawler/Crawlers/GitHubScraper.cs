﻿using ChpCrawler.db;
using ChpCrawler.Models;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Threading;

namespace ChpCrawler.Crawlers
{
    // https://docs.github.com/en/rest
    class GitHubScraper : IScraper
    {
        private readonly CodeHostingPlatformEnum DeafultPlatform = CodeHostingPlatformEnum.GitHub;

        private string ProjectsUrl;
        private readonly string IssuesUrl = $"search/issues?q=repo:*REPONAME*+type:issue";
        private readonly string IssuesTypeUrl = $"+state:*ISSUESTATE*";
        private readonly string RepoUrl = $"repos/*OWNER*/*REPONAME*";
        private readonly string RepoIdUrl = $"repositories/*REPOID*";
        private readonly string ContributorsUrl = $"/contributors?per_page=1&page=*PAGENUM*";

        private int LIMIT;
        private string LIMIT_DATE;
        private int LIMIT_ISSUES;
        private int LIMIT_STARS;
        private int PROJECTS_PER_PAGE;
        private int SLEEP_TIME;

        public string GitHubAccessToken { get; set; }
        public string GitHubUser { get; set; }
        public string BaseUrl { get; set; }
        public IDatabase Storage { get; set; }

        public void Config(int page_limit, int issue_count_limit, int stars_count_limit, string date_limit, int projects_per_page, int sleep_time)
        {
            LIMIT = page_limit;
            LIMIT_DATE = date_limit;
            LIMIT_ISSUES = issue_count_limit;
            LIMIT_STARS = stars_count_limit;
            PROJECTS_PER_PAGE = projects_per_page;
            SLEEP_TIME = sleep_time;
            ProjectsUrl = $"repositories?page=*PAGENUM*&per_page={projects_per_page}";
        }

        public void ExtractProjects(bool use_issue_num_limit = true, bool use_only_starred = true)
        {
            int limit = LIMIT;
            bool canContinue = true;
            int pagenum = 1;

            while (canContinue && limit != 0)
            {
                try
                {
                    string json = GetJson(
                        UrlHelper.BuildUrl(new string[] {
                            BaseUrl,
                            ProjectsUrl.Replace("*PAGENUM*", pagenum.ToString())
                        })
                    );
                    
                    dynamic jsonObj = JsonConvert.DeserializeObject(json);

                    foreach (var obj in jsonObj)
                    {
                        Project project;

                        try
                        {
                            project = FillProject(obj);
                        }
                        catch (System.Net.WebException ex)
                        {
                            Console.WriteLine(ex.Message);
                            Thread.Sleep(SLEEP_TIME);
                            continue;
                        }


                        Thread.Sleep(SLEEP_TIME);
                        if (use_issue_num_limit && project.NumOfIssuesAll < LIMIT_ISSUES) continue;
                        if (use_only_starred && project.NumOfStars < LIMIT_STARS) continue;

                        Storage.Insert(project);
                    }
                    limit--;
                    pagenum++;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    canContinue = false;
                }
            }
        }

        private Project FillProject(dynamic obj)
        {
            Project project = new Project()
            {
                Id = Convert.ToString(obj.id),
                RepoName = Convert.ToString(obj.name),
                CodeHostingPlatform = DeafultPlatform,
                Url = Convert.ToString(obj.html_url),
                OwnerName = obj["owner"] != null ? Convert.ToString(obj["owner"].login) : ""
            };

            GetProjectInfo(project,
                UrlHelper.BuildUrl(new string[] {
                        BaseUrl,
                        RepoIdUrl.Replace("*REPOID*", project.Id)
                })
            );

            GetIssueStatistis(project);
            GetNumOfReleases(project);
            GetNumOfCommits(project);
            GetContributersCount(project);

            return project;
        }

        private void GetContributersCount(Project project)
        {
            int count = 0;
            int page_count = 1;
            string json;
            string url = UrlHelper.BuildUrl(new string[] {
                        BaseUrl,
                        RepoUrl.Replace("*OWNER*", project.OwnerName).Replace("*REPONAME*", project.RepoName)
            });

            while (true)
            {
                json = GetJson(url + ContributorsUrl.Replace("*PAGENUM*", page_count.ToString()));
                dynamic jsonObj = JsonConvert.DeserializeObject(json);
                if (jsonObj.Count == 0) break;
                count++;
                page_count++;
            }

            project.NumOfContributors = count;
        }

        private void GetProjectInfo(Project project, string url)
        {
            string json = GetJson(url);

            dynamic obj = JsonConvert.DeserializeObject(json);

            project.License = obj.license != null ? Convert.ToString(obj.license.name) : "";
            project.NumOfStars = obj.stargazers_count != null ? Convert.ToInt32(obj.stargazers_count) : 0;
            project.NumOfForks = obj.forks_count != null ? Convert.ToInt32(obj.forks_count) : 0;
        }
        
        private void GetNumOfCommits(Project project)
        {
            HtmlWeb web = new HtmlWeb();
            int result = 0;
            HtmlDocument document = web.Load(project.Url);
            if (document != null)
            {
                HtmlNode node = document.DocumentNode.SelectSingleNode($"//a[@class='pl-3 pr-3 py-3 p-md-0 mt-n3 mb-n3 mr-n3 m-md-0 Link--primary no-underline no-wrap']");
                string value = node != null ? node.InnerText.Replace("\n", string.Empty).Replace(" ", string.Empty).Replace("commits", string.Empty) : "0";
                result = Int32.Parse(value.Replace(",", ""));
            }

            project.NumOfCommit = result;
        }

        private void GetNumOfReleases(Project project)
        {
            HtmlWeb web = new HtmlWeb();
            int result = 0;
            string fullname = project.OwnerName + "/" + project.RepoName + "/releases";
            HtmlDocument document = web.Load(project.Url);
            if (document != null)
            {
                HtmlNode node = document.DocumentNode.SelectSingleNode($"//a[contains(@href,'{fullname}')]/span[@class='Counter']");
                string value = node != null ? node.InnerText : "0";
                result = Int32.Parse(value.Replace(",", ""));
            }

            project.NumOfReleases = result;
        }

        private void GetIssueStatistis(Project project)
        {
            // All
            string fullname = project.OwnerName + "/" + project.RepoName;
            string json = GetJson(
                UrlHelper.BuildUrl(new string[] {
                        BaseUrl,
                        IssuesUrl.Replace("*REPONAME*", fullname)
                })
            );
            dynamic jsonObj = JsonConvert.DeserializeObject(json);

            project.NumOfIssuesAll = Convert.ToInt32(jsonObj.total_count);

            // Close
            json = GetJson(
                UrlHelper.BuildUrl(new string[] {
                        BaseUrl,
                        IssuesUrl.Replace("*REPONAME*", fullname) + IssuesTypeUrl.Replace("*ISSUESTATE*", "closed")
                })
            );
            jsonObj = JsonConvert.DeserializeObject(json);

            project.NumOfIssuesOpen = Convert.ToInt32(jsonObj.total_count);

            // Open
            json = GetJson(
                UrlHelper.BuildUrl(new string[] {
                        BaseUrl,
                        IssuesUrl.Replace("*REPONAME*", fullname) + IssuesTypeUrl.Replace("*ISSUESTATE*", "open")
                })
            );
            jsonObj = JsonConvert.DeserializeObject(json);

            project.NumOfIssuesClose = Convert.ToInt32(jsonObj.total_count);
        }

        public string GetJson(string url)
        {
            string json;

            var httpRequest = (HttpWebRequest)WebRequest.Create(url);

            httpRequest.Accept = "application/json";
            httpRequest.UserAgent = GitHubUser;
            httpRequest.Headers["Authorization"] = $"Bearer {GitHubAccessToken}";

            var httpResponse = (HttpWebResponse)httpRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                json = streamReader.ReadToEnd();
            }
            return json;
        }
    }
}
