﻿using ChpCrawler.Models;

namespace ChpCrawler
{
    interface IScraper
    {
        void Config(int page_limit, int issue_count_limit, int stars_count_limit, string date_limit, int projects_per_page, int sleep_time);

        void ExtractProjects(bool use_issue_num_limit = true, bool use_only_starred = true);
    }
}
