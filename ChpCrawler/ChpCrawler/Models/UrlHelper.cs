﻿using System;

namespace ChpCrawler.Models
{
    class UrlHelper
    {
        public static string BuildUrl(string[] strings)
        {
            string result = "";

            foreach (string str in strings)
            {
                if (str[0] == '/')
                {
                    str.Remove(0, 1);
                }
                if (str[str.Length - 1] != '/')
                {
                    result += str + '/';
                }
                else 
                {
                    result += str;
                }
            }

            Console.WriteLine(result);
            return result.Remove(result.Length - 1);
        }
    }
}
