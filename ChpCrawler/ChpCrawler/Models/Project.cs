﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChpCrawler.Models
{
    enum CodeHostingPlatformEnum
    { 
        GitHub,
        GitLab
    }
    
    class Project
    {
        public string Id { get; set; }
        public string OwnerName { get; set; }
        public string RepoName { get; set; }
        public CodeHostingPlatformEnum CodeHostingPlatform { get; set; }
        public string Url { get; set; }
        public string License { get; set; }
        public int NumOfCommit { get; set; }
        public int NumOfContributors { get; set; }
        public int NumOfIssuesAll { get; set; }
        public int NumOfIssuesOpen { get; set; }
        public int NumOfIssuesClose { get; set; }
        public int NumOfStars { get; set; }
        public int NumOfForks { get; set; }
        public int NumOfReleases { get; set; }
    }

}
