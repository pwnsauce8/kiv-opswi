﻿using ChpCrawler.Models;

namespace ChpCrawler.db
{
    interface IDatabase
    {
        void Insert(Project project);
    }
}
