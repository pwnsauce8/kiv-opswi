﻿using ChpCrawler.db;
using Notion.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChpCrawler.Models
{
    // https://github.com/notion-dotnet/notion-sdk-net/
    class NotionDatabase : IDatabase
    {
        private NotionClient Client;
        public string NotionDatabaseId { get; set; } 
        public string AuthToken { get; set; }
        public string NotionBaseUrl { get; set; }

        public async Task GetDatabaseData()
        {
            var client = NotionClientFactory.Create(new ClientOptions
            {
                AuthToken = this.AuthToken
            });

            var databasesQueryParameters = new DatabasesQueryParameters();
            var queryResult = await client.Databases.QueryAsync(NotionDatabaseId, databasesQueryParameters);

            foreach (var result in queryResult.Results)
            {
                Console.WriteLine("Page Id: " + result.Id);
                foreach (var property in result.Properties)
                {
                    Console.WriteLine(property.Key + " " + GetValue(property.Value));
                }
            }
        }

        internal void Auth()
        {
            Client = NotionClientFactory.Create(new ClientOptions
            {
                AuthToken = this.AuthToken
            });
        }

        public async Task UpdateProperty()
        {
           var databasesUpdateParameters = new DatabasesUpdateParameters();
            Dictionary<string, IUpdatePropertySchema> keyValuePairs = new Dictionary<string, IUpdatePropertySchema>();

            databasesUpdateParameters.Properties = new Dictionary<string, IUpdatePropertySchema>
            {
                { "id", new TitleUpdatePropertySchema { Title = new Dictionary<string, object>() } },
                { "Price", new NumberUpdatePropertySchema { Number = new Number { Format = "yen" } } },
                { "Food group", new SelectUpdatePropertySchema
                    {
                        Select = new OptionWrapper<SelectOption>
                        {
                            Options = new List<SelectOption>
                            {
                                new SelectOption
                                {
                                    Color = "green",
                                    Name = "🥦Vegetables"
                                },
                                new SelectOption
                                {
                                    Color = "red",
                                    Name = "🍎Fruit"
                                },
                                new SelectOption
                                {
                                    Color = "yellow",
                                    Name = "💪Protein"
                                }
                            }
                        }
                    }
                },
                { "Last ordered", new DateUpdatePropertySchema{ Date = new Dictionary<string, object>() } }
            };

            var queryResult = await Client.Databases.UpdateAsync(NotionDatabaseId, databasesUpdateParameters);

        }

        public async Task<Page> InsertAsync(Project project)
        {
            var pagesCreateParameters = PagesCreateParametersBuilder.Create(new DatabaseParentInput
            {
                DatabaseId = NotionDatabaseId
            }).AddProperty("id", new TitlePropertyValue()
            {
                Title = new List<RichTextBase>()
                {
                    new RichTextText()
                    {
                        Text = new Text
                        {
                            Content = project.Id
                        }
                    }
                }
            }).AddProperty("Code hosting platform", new SelectPropertyValue()
            {
                Select = new SelectOption
                {
                    Name = project.CodeHostingPlatform.ToString()
                }
            }).AddProperty("Repo name", new RichTextPropertyValue()
            {
                RichText = new List<RichTextBase>()
                {
                    new RichTextText()
                    {
                        Text = new Text
                        {
                            Content = project.RepoName
                        }
                    }
                }
            }).AddProperty("Owner name", new RichTextPropertyValue()
            {
                RichText = new List<RichTextBase>()
                {
                    new RichTextText()
                    {
                        Text = new Text
                        {
                            Content = project.OwnerName
                        }
                    }
                }
            }).AddProperty("License", new RichTextPropertyValue()
            {
                RichText = new List<RichTextBase>()
                {
                    new RichTextText()
                    {
                        Text = new Text
                        {
                            Content = project.License
                        }
                    }
                }
            }).AddProperty("# commits", new NumberPropertyValue()
            {
                Number = project.NumOfCommit
            }).AddProperty("# issues (all)", new NumberPropertyValue()
            {
                Number = project.NumOfIssuesAll
            }).AddProperty("# issues (close)", new NumberPropertyValue()
            {
                Number = project.NumOfIssuesClose
            }).AddProperty("# issues (open)", new NumberPropertyValue()
            {
                Number = project.NumOfIssuesOpen
            }).AddProperty("# stars (watchers)", new NumberPropertyValue()
            {
                Number = project.NumOfStars
            }).AddProperty("# contributors", new NumberPropertyValue()
            {
                Number = project.NumOfContributors
            })
            .AddProperty("Url", new UrlPropertyValue()
            {
                Url = project.Url != null ? project.Url : ""
            })
            .Build();

            Page page = await Client.Pages.CreateAsync(pagesCreateParameters);
            return page;
        }


        object GetValue(PropertyValue p)
        {
            switch (p)
            {
                case RichTextPropertyValue richTextPropertyValue:
                    return richTextPropertyValue.RichText.FirstOrDefault()?.PlainText;
                default:
                    return null;
            }
        }

        void IDatabase.Insert(Project project)
        {
            Task<Page> upload = InsertAsync(project);

            if (!upload.IsFaulted)
            {
                Page page = upload.Result;
            }
            else
            {
                Console.WriteLine(upload.Exception.Message);
            }
        }
    }
}
