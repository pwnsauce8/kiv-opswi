﻿using ChpCrawler.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;

namespace ChpCrawler.db
{
    class CsvExport : IDatabase
    {
        private List<Project> Projects;
        private string Filename;

        public CsvExport(string filename) 
        {
            Projects = new List<Project>();
            Filename = filename;
        }

        public void Insert(Project project)
        {
            Projects.Add(project);
        }

        public void WriteToFile()
        {
            var lines = new List<string>();

            // Create header
            IEnumerable<PropertyDescriptor> props = TypeDescriptor.GetProperties(typeof(Project)).OfType<PropertyDescriptor>();
            var header = string.Join(",", props.ToList().Select(x => x.Name));
            lines.Add(header);


            var valueLines = Projects.Select(row => string.Join(",", header.Split(',').Select(a => row.GetType().GetProperty(a).GetValue(row, null))));
            lines.AddRange(valueLines);

            //after your loop
            File.WriteAllText(Filename, String.Join("\n", lines.ToArray()));
        }
    }
}
