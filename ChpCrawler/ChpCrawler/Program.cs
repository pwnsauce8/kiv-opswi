﻿using ChpCrawler.Crawlers;
using ChpCrawler.db;
using System;
using System.Configuration;

namespace ChpCrawler
{
    class Program
    {
        private static int PageLimit;
        private static string DateLimit;
        private static int LimitIssues;
        private static int LimitStars;
        private static int ProjectsPerPage;
        private static int SleepTime;
        private static string GithubUsername;
        private static int RunGitHub;
        private static int RunGitLab;
        private static string GitHubAccessToken;
        private static string GitLabAccessToken;
        private static string CsvExportPath;

        static void Main(string[] args)
        {
            ReadConfigs();

            //NotionDatabase notion = new NotionDatabase()
            //{
            //    NotionDatabaseId = "20b1b004efdb4763b1bb7f52853d8e39",
            //    AuthToken = "secret_Y2GQHrMPDHwZv0c4GP6ZfGA1EX3NatEiCkUitjbxqdC",
            //    NotionBaseUrl = "https://api.notion.com"
            //};
            //notion.Auth();

            CsvExport csvExport = new CsvExport(CsvExportPath);

            if (RunGitHub == 1)
            {
                GitHubScraper github = new GitHubScraper()
                {
                    GitHubAccessToken = GitHubAccessToken,
                    BaseUrl = "https://api.github.com/",
                    Storage = csvExport,
                    GitHubUser = GithubUsername
                };

                github.Config(page_limit: PageLimit, issue_count_limit: LimitIssues, stars_count_limit: LimitStars, date_limit: DateLimit, projects_per_page: ProjectsPerPage, sleep_time: SleepTime);
                github.ExtractProjects(true, true);
            }

            if (RunGitLab == 1)
            {
                GitLabScraper gitLab = new GitLabScraper()
                {
                    GitLabAccessToken = GitLabAccessToken,
                    BaseUrl = "https://gitlab.com/api/v4/",
                    Storage = csvExport
                };

                gitLab.Config(page_limit: PageLimit, issue_count_limit: LimitIssues, stars_count_limit: LimitStars, date_limit: DateLimit, projects_per_page: ProjectsPerPage, sleep_time: SleepTime);
                gitLab.ExtractProjects(true, true);
            }

            // Export to csv
            csvExport.WriteToFile();

            Console.WriteLine("Finish");
            Console.ReadLine();
        }

        private static void ReadConfigs()
        {
            string pageLimit = ConfigurationManager.AppSettings.Get("PageLimit");
            string limitIssues = ConfigurationManager.AppSettings.Get("LimitIssues");
            string limitStars = ConfigurationManager.AppSettings.Get("LimitStars");
            string projectsPerPage = ConfigurationManager.AppSettings.Get("ProjectsPerPage");
            string sleepTime = ConfigurationManager.AppSettings.Get("SleepTime");
            string runGitHub = ConfigurationManager.AppSettings.Get("RunGitHub");
            string runGitLab = ConfigurationManager.AppSettings.Get("RunGitLab");

            GitHubAccessToken = ConfigurationManager.AppSettings.Get("GitHubAccessToken");
            GitLabAccessToken = ConfigurationManager.AppSettings.Get("GitLabAccessToken");
            CsvExportPath = ConfigurationManager.AppSettings.Get("CsvExportPath");
            PageLimit = Int32.Parse(pageLimit);
            DateLimit = ConfigurationManager.AppSettings.Get("DateLimit");
            LimitIssues = Int32.Parse(limitIssues);
            LimitStars = Int32.Parse(limitStars);
            ProjectsPerPage = Int32.Parse(projectsPerPage);
            SleepTime = Int32.Parse(sleepTime);
            GithubUsername = ConfigurationManager.AppSettings.Get("GithubUsername");
            RunGitHub = Int32.Parse(runGitHub);
            RunGitLab = Int32.Parse(runGitLab);
        }

    }
}
